package Week2Day2;

public interface TV {
 public void changeChannel(int channelnumber);
 public void changeChannel(String channelname);
 public void powerOff(String button);
 
}
