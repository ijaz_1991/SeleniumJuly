package projectDay;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class FBSearchTestLeaf extends Facebook_Method {
	@Test
	public void search() throws InterruptedException
	{
		login();
		WebElement eleSearch = locateElement("xpath","//input[@class='_1frb']");
		type(eleSearch, "Testleaf");
		WebElement eleSclick = locateElement("xpath","//button[@aria-label='Search']/i ");
		click(eleSclick);
		//WebElement eleLike = locateElement("xpath","(//button[@class='_42ft _4jy0 PageLikeButton _4jy3 _517h _51sy'])[1]");
		WebElement eleLike = locateElement("xpath","(//button[@type='submit'])[2]");
		String txt1 = getText(eleLike);
		if(txt1.equals("Like"))
		{
			click(eleLike);
		}
		else if(txt1.equals("Liked"))
			System.out.println("It is already liked");
		click(eleLike);
		WebElement eleTclick = locateElement("xpath","//div[text()='TestLeaf']");
		click(eleTclick);
		String title = driver.getTitle();
		System.out.println(title);
		if(title.contains("TestLeaf")||title.contains("Testleaf"))
		{
			System.out.println("TestLeaf is entered successfully");
		}
		else
			System.out.println("TestLeaf is not entered");
		WebElement eleLikecount = locateElement("xpath","(//div[@class='_4bl9'])[4]");
		String txt2 = getText(eleLikecount);
		System.out.println(txt2);
		driver.close();
	}

}
