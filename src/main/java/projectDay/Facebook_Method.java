package projectDay;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import Week5Day1.SEMethods;


public class Facebook_Method extends SEMethods {

	@BeforeMethod
	public void login() {

		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUserName = locateElement("id", "email");
		type(eleUserName, "ijazmohamed.cool@gmail.com");
		WebElement elePassword = locateElement("id","pass");
		type(elePassword, "");
		WebElement eleLogin = locateElement("xpath"," //input[@type='submit']");
		click(eleLogin);
	}

	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="FB_LOGIN";
		category = "Smoke";
		author= "SACHIN";
	}
		
	
	@AfterMethod
	public void close() {

		closeAllBrowsers();

	}

}