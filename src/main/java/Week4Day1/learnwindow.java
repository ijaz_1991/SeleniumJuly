package Week4Day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class learnwindow {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//Will get window ID
		String windowHandle=driver.getWindowHandle();
		System.out.println(windowHandle);
		Set<String> allwindows=driver.getWindowHandles();
		System.out.println(allwindows.size());
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows1=driver.getWindowHandles();
		System.out.println(allwindows1.size());
		String title=driver.getTitle();
		System.out.println(title);
		List<String> listofWindows=new ArrayList<String>();
		listofWindows.addAll(allwindows1);
		String secondwindow=listofWindows.get(1);
		driver.switchTo().window(secondwindow);
		String newtitle=driver.getTitle();
		System.out.println(newtitle);
		driver.quit();





	}

}
