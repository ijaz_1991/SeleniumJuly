package Week4Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseActions {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/draggable/");
		driver.switchTo().frame(0);
		WebElement drag=driver.findElementByXPath("//div[@id='draggable']/p");
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(drag, 100, 150).perform();
	}

}
