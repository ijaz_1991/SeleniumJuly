package Week2Day3;

public class Exception {

	public static void main(String[] args) {
		try {

			int[] arr;
			arr=new int[5];
			arr[0]=5;
			arr[1]=10;
			arr[2]=15;
			arr[3]=20;
			arr[4]=25;
			System.out.println("Value at sixth position is "+arr[5]);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Should not enter array value greater than 4");
		}
	}
}



