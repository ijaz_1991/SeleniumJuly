package testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC002_CreateLead extends SeMethods {
	@Test
	public void createLead() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrmsfa= locateElement("linktext", "CRM/SFA");
		click(elecrmsfa);

		WebElement elecreat= locateElement("linktext", "Create Lead");
		click(elecreat);
		WebElement elecompanyname= locateElement("id", "createLeadForm_companyName");
		type(elecompanyname, "cts");

		WebElement elefirstname= locateElement("id", "createLeadForm_firstName");
		type(elefirstname, "ijaz");

		WebElement elelastname= locateElement("id", "createLeadForm_lastName");
		type(elelastname, "mohammad");
		WebElement elecurrency=locateElement("id","createLeadForm_currencyUomId");
		selectDropDownUsingText(elecurrency,"NZD - New Zealand Dollar");

		WebElement elesubmit= locateElement("class", "smallSubmit");
		click(elesubmit);
		//WebElement eleverify=locateElement("xpath","//span[@id=\'viewLead_firstName_sp\']");
		//System.out.println(eleverify.getAttribute("textcontent"));



	}





}
