package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC003_Mergelead extends SeMethods {
	@Test
	public void createLead() throws InterruptedException  {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrmsfa= locateElement("linktext", "CRM/SFA");
		click(elecrmsfa);
		WebElement elelead=locateElement("linktext","Leads");
		click(elelead);
		WebElement elemerg=locateElement("linktext","Merge Leads");
		click(elemerg);
		WebElement elefrom=locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(elefrom);
		Thread.sleep(10000);
		WebElement activeElement=driver.switchTo().activeElement();
		WebElement eleleadid=locateElement("xpath","(//input[@type=\"text\"])[1]");
		type(eleleadid,"1234");

		WebElement elefind=locateElement("xpath","//button[text()='Find Leads']");
		click(elefind);




	}
}

