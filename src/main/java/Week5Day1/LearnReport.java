package Week5Day1;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;




public class LearnReport {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testcasename,testcaseDescription,author,category,excelfileName;

	@BeforeSuite //one time execution
	public void startResult() {
		//create uneditable html file
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/result.html");
		//history
		html.setAppendExisting(true);

		//editable

		extent = new ExtentReports();
		extent.attachReporter(html);
	}

	@BeforeMethod
	public void startTestcase() {
		test=extent.createTest(testcasename, testcaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);

	}

	public void reportStep(String desc,String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		} if(status.equalsIgnoreCase("fail")) {
			test.pass(desc);
		}

	}
	public void flush() {
		extent.flush();
	}
}





//	public static void main(String[] args) throws IOException {
//		ExtentTest test=extent.createTest("TC001_CreateLead", "Create Lead");
//		test.assignAuthor("ijaz");
//		test.assignCategory("smoke");
//		test.pass("The Data DemoSalesManager is Entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img1.png").build());
//		test.pass("Login Button clicked successfully");
//		extent.flush();
//
//
//	}
//
//}
