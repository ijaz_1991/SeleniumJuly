package Week5Day1;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import wdMethods.WdMethods;

public class SEMethods implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	ChromeOptions op;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "name"  : return driver.findElementByName(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "ltext" : return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		//System.out.println("Like Text"+text);
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String actualTitle = driver.getTitle();
		try {
			//String st = ele.getAttribute("textContent");
			if(actualTitle.equals(expectedTitle))
			{
				//System.out.println(ele.getAttribute("textContent"));	
				System.out.println("The data "+expectedTitle+" is Entered Successfully");
				return true;
			}
			else
				System.out.println("The data "+expectedTitle+" is not Entered");
			return false;
		} catch (WebDriverException e) {
			System.out.println("The data "+expectedTitle+" is Not Entered");
			return false;
		} finally {
			takeSnap();
		}}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String st = ele.getAttribute("textContent");
			if(st.equals(expectedText))
			{
				//System.out.println(ele.getAttribute("textContent"));
				System.out.println("The data "+expectedText+" is Entered Successfully");
			}
			else
				System.out.println("The data "+expectedText+" is not Entered");
		} catch (WebDriverException e) {
			System.out.println("The data "+expectedText+" is Not Entered");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String st = ele.getAttribute("textContent");
			System.out.println(ele.getAttribute("textContent"));
			if(st.contains(expectedText))
			{
				//System.out.println(ele.getAttribute("textContent"));
				System.out.println("The data "+expectedText+" is Entered Successfully");
			}
			else
				System.out.println("The data "+expectedText+" is not Entered");
		} catch (WebDriverException e) {
			System.out.println("The data "+expectedText+" is Not Entered");
		} finally {
			takeSnap();
		}


	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> getWinCount=new ArrayList<String>();
		getWinCount.addAll(windowHandles);
		String Win = getWinCount.get(index);
		driver.switchTo().window(Win);
		//System.out.println("Second Window Title :"+driver.getTitle());
		//String currentUrl = driver.getCurrentUrl();
		//System.out.println("Current Window URL is :"+currentUrl);
		//driver.quit();
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		//driver.switchTo().alert().sendKeys("Learn Alert");
		String text = driver.switchTo().alert().getText();
		//System.out.println(text);
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}